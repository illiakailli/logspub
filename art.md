# notes related to art and history of art

 - art
  - derives from the Latin word “ars” (stem: art-), meaning “skill,” “craft,” or “technique” 
  - in classical Latin, it referred broadly to any skill or craft acquired through learning or practice
  - indo-european root “ar-” or “arə-”, meaning “to fit together” or “join”
    - reflects the idea of craftsmanship and creation


## periods and significant works

- prehistoric (before 3000 bce)
	- lascaux cave paintings (france)
	- the shaft of the dead man
		- this scene portrays a human figure with a bird-like head alongside a wounded bison
		- offering insight into early symbolic representation
	- venus of willendorf (limestone figurine)
	- altamira cave paintings (spain)

- ancient (3000 bce–500 ce)
	- great pyramids of giza (egyptian architecture)
	- parthenon sculptures (greek classical art)
	- augustus of prima porta (roman statue)
	- stele of hammurabi (babylonian relief)

- medieval (500–1400)
	- bayeux tapestry (norman conquest embroidery)
	- hagia sophia mosaics (byzantine art)
	- chartres cathedral stained glass (gothic design)
	- book of kells (illuminated manuscript)

- renaissance (1400–1600):
	- mona lisa (leonardo da vinci)
	- the creation of adam (michelangelo, sistine chapel)
	- the school of athens (raphael)
	- david (michelangelo sculpture)

Baroque (1600–1750)
	•	The Night Watch (Rembrandt)
	•	The Ecstasy of Saint Teresa (Bernini sculpture)
	•	Las Meninas (Diego Velázquez)
	•	Judith Slaying Holofernes (Artemisia Gentileschi)

Romanticism (1750–1850):
	•	The Raft of the Medusa (Théodore Géricault)
	•	Liberty Leading the People (Eugène Delacroix)
	•	Wanderer above the Sea of Fog (Caspar David Friedrich)
	•	The Third of May 1808 (Francisco Goya)

Modern (1850–1950):
	•	Starry Night (Vincent van Gogh)
	•	Les Demoiselles d’Avignon (Pablo Picasso)
	•	The Persistence of Memory (Salvador Dalí)
	•	Impression, Sunrise (Claude Monet)

Contemporary (1950–Present):
	•	Campbell’s Soup Cans (Andy Warhol)
	•	Untitled (1982) (Jean-Michel Basquiat)
	•	The Physical Impossibility of Death in the Mind of Someone Living (Damien Hirst)
	•	Spiral Jetty (Robert Smithson, Earthwork)


## people

- микеланджело буонарроти (1475–1564)
  - сотворение адама
    - allusions
      - macaroni monster
        - пародийная религия, известная как пастафарианство
        - potentially a reference to benito mussolini
  - страшный суд
  - пьета: скульптура богоматери и христа

- rembrandt van rijn (1606–69)
  - was a dutch baroque painter and printmaker
  - balaam and the ass (1626)
    - painting by rembrandt van rijn
    - painting portrays the biblical account of the talking ass debating with diviner balaam
    - о покорном, молчаливом человеке, который неожиданно запротестовал или выразил свое мнение


## theatre

- phantasmagoria 
  - was a form of horror theatre that (among other techniques) used one or more magic lanterns to project frightening images, such as skeletons, demons, and ghosts, onto walls, smoke, or semi-transparent screens, typically using rear projection to keep the lantern out of sight
  - originated in late 18th century europe
  - an early form: zouma deng, or "trotting horse lamp" 
    - this device used a cylindrical lantern with images that would appear to move as the heat from a candle inside turned a rotor. It was primarily used in entertainment and storytelling, projecting figures of horses, people, or animals onto surfaces, creating a magical illusion of motion

