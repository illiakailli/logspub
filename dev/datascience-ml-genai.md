# notes related to genai

## top models (as of jan 2025)

- text generation modality
  - general purpose
    - closed-source
      - gpt-4 (openai), gemini 1.5 (google), claude 3 (anthropic)
    - open weights
      - llama (meta), mistral, dbrx (databricks)
      - deepseek (R models are dense, V models are MoE)
  - code assistaint
    - codex (microsoft/openai, copilot)
    - codewhisperer (amazon)
    - tabnine
    - code llama (meta)

- vision modality
  - image generation
    - closed source
      - dall-e 3 (openai)
      - midjourney
    - open weights
      - stable diffusion 3 (stability ai)

  - video generation
    - sora (openai)
    - runway gen-3

- audio modality
  - speech to text: whisper
  - voice synthesis
    - elevenlabs: high-quality tts/voice cloning
    - openai voice engine (preview): ethical voice replication
    - meta voicebox: research-only, versatile speech generation

  - music
    - Suno AI v3: Viral song generation (lyrics + vocals)
    - Stable Audio 2.0: Commercial-safe music/sound effects

  - sound effects
    - audiocraft (meta): open-source sound/music tools

- multimodal
  - gpt-4o (openai): real-time audio/text/vision integration


## common architectures/patterns

- self-attention layers
  - enable each token in a sequence to focus on and weight the importance of other tokens, allowing the model to capture contextual relationships

- transformers
  - combine tokenizer, self-attention and feed-forward modules in a single model
  - enable context-aware understanding
  - most compute gets spent on self-attention and feed-forward layer
    - self-attention being dependent on number of tokens fed into a context window
    - feed-forward is constant for every next token inference

- diffusion
  - gradually denoise data via reverse diffusion to generate high-fidelity outputs
  - while denoising, augment with metainformation labels for training

- dense vs mixture of experts

- retrieval-augmented generation
  - augmenting prompt with custom data found via similarity search typically in a vector base


## embeddings [4]

- they are
  - learned low-dimensional representations of discrete data as continuous vectors
  - overcome limitations of one-hot encoding
  - can be extracted from model and reused to speed-up training in some situations

- can be
  - static
    - word2vec or glove
    - does not encode information about the context
  - contextual
    - encode information about the context
    - examples: ollama mxbai-embed-large, nomic-embed-text, and all-minilm


## terminology

- BERT models (Bidirectional Encoder Representations from Transformers)
  - uses contextual information from both sides of the token for training and inference
  - especially effective for gap-filling tasks


## references

[4]: https://towardsdatascience.com/neural-network-embeddings-explained-4d028e6f0526
